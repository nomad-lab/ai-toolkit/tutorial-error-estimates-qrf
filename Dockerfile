FROM gitlab-registry.mpcdf.mpg.de/nomad-lab/analytics:production4af585a9


COPY --chown=${NB_UID}:${NB_GID} assets/ ./tutorials/assets/
COPY --chown=${NB_UID}:${NB_GID} data/ ./tutorials/data/
COPY --chown=${NB_UID}:${NB_GID} cbs_with_qrf.ipynb ./tutorials/
