# Analytics Error Estimates QRF

Welcome to the juptyer notebook where we implement a quantile random forest (QRF) to use for total energy delta learning on DFT data. This means, we train our QRF model to predic the energy difference in low precision calculation to a high precision calculation, the energy delta/deviation/error if you will. The QRF model is compared to the stoichiometric model from Ref. [1](https://www.nature.com/articles/s41524-022-00744-4). To understand more about the QRF model used here and the task at hand please refer to our paper which you can find at this clickable link [here](https://arxiv.org/abs/2303.14760).

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Installation
The notebook is made to be self-contained. As a result only a few python libraries are needed, namely: matplotlib, numpy, pandas, scikit-learn and scipy.

## Usage
The models in this paper can be applied to DFT data from other codes and also
non materials science related applications. Feel free to use the implementation
of the quantile random forest model.

## Support
For support feel free to e-mail Daniel Speckhard at speckhard@fhi.mpg.de

## Contributing
We are very much open to contibutions.